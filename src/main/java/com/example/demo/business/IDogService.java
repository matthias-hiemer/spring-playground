package com.example.demo.business;

import com.example.demo.model.Dog;
import java.util.NoSuchElementException;
import java.util.Optional;

public interface IDogService {

    Optional<Dog> getById(Long id) throws NoSuchElementException;

    Iterable<Dog> getDogs();

    Dog createDog(Dog dog);

    Dog generateRandomDog();
}
