package com.example.demo.business.impl;

import com.example.demo.api.PhotoFileConversionException;
import com.example.demo.business.IPhotoBiz;
import com.example.demo.model.Photo;
import com.example.demo.repo.DogRepository;
import com.example.demo.repo.PhotoRepository;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@Service
public class PhotoBiz implements IPhotoBiz {

    private final PhotoRepository photoRepo;
    private final DogRepository dogRepo;

    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss-SSS");

    @Value("${com.example.demo.photo.location}")
    String fileDirectory;

    @Autowired
    public PhotoBiz(PhotoRepository photoRepo, DogRepository dogRepo) {
        this.photoRepo = photoRepo;
        this.dogRepo = dogRepo;
    }

    @Override
    public List<Photo> getPhotosByDog(Long dogId) {
        return photoRepo.findAllByDogId(dogId);
    }

    @Override
    public Optional<Photo> getById(Long id) throws NoSuchElementException {
        return photoRepo.findById(id);
    }

    @Override
    public byte[] getPhotoData(String fileName) throws PhotoFileConversionException, NoSuchFileException {

        // Get photo entity by ID
        Photo photoEntity = photoRepo
            .findByFileName(fileName)
            .orElseThrow(() -> new NoSuchElementException("photo not found! filename: " + fileName));

        Path path = Path.of(fileDirectory + photoEntity.getFileName());

        // Check if file exists
        if (!Files.exists(path)) {
            log.error("The Photo entity (metadata) for fileName " + fileName
                + " exists, but the image file could not be found on file system! Path: " + path);
            throw new NoSuchFileException("The Photo Entity for fileName " + fileName
                + " exists, but the image file could not be found on file system! Path: " + path);
        }

        // Convert to byte array
        try {
            byte[] imageAsByte = Files.readAllBytes(path);
            log.info("Load image from file system: " + path);

            return imageAsByte;

        } catch (IOException e) {
            throw new PhotoFileConversionException(
                "Error while converting the photo file to byte array! fileName: " + fileName + " path: " + path, e);
        }
    }

    @Override
    @Transactional
    public Photo createPhoto(Long dogId, MultipartFile multipartFile) {

        // Check: Does dog exist?
        if (!dogRepo.existsById(dogId)) {
            throw new NoSuchElementException(
                "Could not persist photo for dog! The dog with given ID does not exist! id: " + dogId);
        }

        // save jpeg metadata
        // Filename example: 2021-01-12-10-53-25-061_1001.jpeg
        String fileEnding = ".jpg";
        String fileName = LocalDateTime.now().format(dateFormatter) + "_" + dogId + fileEnding;

        Photo photoMetadata = new Photo(fileName, dogId);
        photoMetadata = photoRepo.save(photoMetadata);

        // save jpeg on filesystem
        try {
            Path path = Path.of(fileDirectory + photoMetadata.getFileName());
            multipartFile.transferTo(Files.createFile(path));

            log.info("Saved new jpeg under: " + path);

        } catch (IOException e) {
            log.error("Could not delete photo! photo id: " + photoMetadata.getId() +
                " fileName: " + photoMetadata.getFileName());
            throw new UncheckedIOException(e);
        }

        return photoMetadata;
    }

    @Override
    @Transactional
    public Long deletePhoto(Long id) throws NoSuchElementException {

        // Remove jpeg from file system
        Photo photo = photoRepo
            .findById(id)
            .orElseThrow(() -> new NoSuchElementException("Photo can't be found and therefore not deleted! id: " + id));

        try {
            Path path = Path.of(fileDirectory + photo.getFileName());

            if (Files.exists(path)) {
                Files.delete(path);

            } else {
                log.warn("The jpeg file (to be deleted) does not exist. " +
                    "This may happen if it has been already deleted manually. " +
                    "Now continuing to delete image metadata.");
            }

        } catch (IOException e) {
            log.error("Could not delete photo! photo id: " + photo.getId() + " fileName: " + photo.getFileName());
            throw new UncheckedIOException(e);
        }

        // Remove metadata from DB
        photoRepo.deleteById(id);

        log.info("Photo metadata deleted! ID: " + id);

        return id;
    }
}

