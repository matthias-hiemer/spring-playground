package com.example.demo.business.impl;

import com.example.demo.business.IDogDataGenerationService;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DogDataGenerationService implements IDogDataGenerationService {

    private static final int AVG_DOG_AGE_IN_YEARS = 12;

    private static final List<String> titles = List.of(
        "Emporer",
        "Master",
        "Professor",
        "Sir",
        "Lady",
        "President",
        "Mister",
        "colonel",
        "His Majesty",
        "King",
        "Queen",
        "Captain",
        "Chief",
        "Pharaoh",
        "Sergeant",
        "Commander",
        "Doctor",
        "Uncle",
        "Saint",
        "Madam",
        "Prince",
        "Officer"

    );

    private static final List<String> names = List.of(
        "Biscuit",
        "Knödel",
        "Fluffers",
        "Bacon",
        "Wigglesworth",
        "Goofball",
        "Begger",
        "Puppins",
        "Snickerdoodle",
        "Butters",
        "Pancakes",
        "Nappington",
        "Sniffer",
        "Slobbers",
        "McPorkchop",
        "Puddles",
        "Floofer",
        "Doozer",
        "Barkington",
        "Bella",
        "Luna",
        "Lucy",
        "Daisy",
        "Lola",
        "Molly",
        "Bailey",
        "Stella",
        "Charlie",
        "Cooper",
        "Buddy",
        "Bear",
        "Rocky",
        "Duke",
        "Tucker"
    );

    @Override
    public String generateName() {
        Random rand = new Random();

        String title = titles.get(rand.nextInt(titles.size()));
        String name = names.get(rand.nextInt(names.size()));

        return title + " " + name;
    }

    @Override
    public LocalDate generateBirthday(){

        LocalDate oldestBirthday = LocalDate.now().minusYears(AVG_DOG_AGE_IN_YEARS);
        long maxDays = ChronoUnit.DAYS.between(oldestBirthday, LocalDate.now());

        return oldestBirthday.plusDays(new Random().nextInt((int) maxDays));
    }

}
