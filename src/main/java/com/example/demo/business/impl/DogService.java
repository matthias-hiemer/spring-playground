package com.example.demo.business.impl;

import com.example.demo.business.IDogService;
import com.example.demo.business.IDogDataGenerationService;
import com.example.demo.model.Dog;
import com.example.demo.repo.DogRepository;
import java.time.LocalDate;
import java.util.NoSuchElementException;
import java.util.Optional;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DogService implements IDogService {

    private final DogRepository dogRepo;
    private final IDogDataGenerationService generationService;

    @Autowired
    public DogService(DogRepository dogRepo, IDogDataGenerationService generationService) {
        this.dogRepo = dogRepo;
        this.generationService = generationService;
    }

    @Override
    public Optional<Dog> getById(Long id) throws NoSuchElementException {
        return this.dogRepo.findById(id);
    }

    @Override
    public Iterable<Dog> getDogs() {
        return dogRepo.findAll(Sort.by(Sort.Direction.ASC, "name"));
    }

    @Override
    public Dog createDog(Dog newDog) throws IllegalArgumentException {

        // validate (may throw exceptions)
        validateNewDog(newDog);

        Dog persistedDog = dogRepo.save(newDog);
        log.info("Created new dog with name {''} and id {}" + persistedDog.getName(), persistedDog.getId());

        return persistedDog;
    }

    @Override
    public Dog generateRandomDog() {
        Dog generatedDog = new Dog()
            .withName(generationService.generateName())
            .withBirthday(generationService.generateBirthday());

        // persist
        return createDog(generatedDog);
    }

    private void validateNewDog(Dog dog) {

        if (dog.getBirthday() != null
            && dog.getBirthday().isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Given birthday is in the future! Birthday: " + dog.getBirthday());
        }


    }

}
