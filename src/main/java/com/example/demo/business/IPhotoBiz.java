package com.example.demo.business;

import com.example.demo.api.PhotoFileConversionException;
import com.example.demo.model.Photo;
import java.nio.file.NoSuchFileException;
import java.util.List;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;

public interface IPhotoBiz {

    List<Photo> getPhotosByDog(Long dogId);

    Optional<Photo> getById(Long id);

    Long deletePhoto(Long id);

    byte[] getPhotoData(String fileName) throws PhotoFileConversionException, NoSuchFileException;

    Photo createPhoto(Long dogId, MultipartFile multipartFile);
}
