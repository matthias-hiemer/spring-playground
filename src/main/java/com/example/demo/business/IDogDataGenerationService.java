package com.example.demo.business;

import java.time.LocalDate;

public interface IDogDataGenerationService {

    String generateName();

    LocalDate generateBirthday();
}
