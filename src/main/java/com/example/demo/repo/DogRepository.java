package com.example.demo.repo;

import com.example.demo.model.Dog;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface DogRepository extends PagingAndSortingRepository<Dog, Long> {

}
