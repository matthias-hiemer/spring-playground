package com.example.demo.repo;

import com.example.demo.model.Photo;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PhotoRepository extends PagingAndSortingRepository<Photo, Long> {

    /**
     * Finds all photos for specific dog
     *
     * @return photos for dog
     */
    List<Photo> findAllByDogId(Long dogId);

    Optional<Photo> findByFileName(String fileName);
}
