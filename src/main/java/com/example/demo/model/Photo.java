package com.example.demo.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "photo")
public class Photo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long dogId;
    private LocalDateTime createdDate;

    private String fileName;

    @Transient
    private String url; // Generated URL to access the file

    public Photo(String fileName, Long dogId) {
        super();
        this.fileName = fileName;
        this.dogId = dogId;
    }

    @PrePersist
    protected void onCreate() {
        this.createdDate = LocalDateTime.now();
    }
}
