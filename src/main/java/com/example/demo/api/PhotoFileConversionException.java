package com.example.demo.api;


public class PhotoFileConversionException extends Exception {

    public PhotoFileConversionException(String message, Exception e) {
        super(message, e);
    }
}
