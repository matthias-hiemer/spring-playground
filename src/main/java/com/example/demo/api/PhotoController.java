package com.example.demo.api;

import com.example.demo.business.IPhotoBiz;
import com.example.demo.model.Dog;
import com.example.demo.model.Photo;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.nio.file.NoSuchFileException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController(value = "PhotoController")
public class PhotoController {

    public static final String PHOTOS_IMAGE_URL = "photos/image/";
    private final IPhotoBiz apiService;

    @Autowired
    public PhotoController(IPhotoBiz apiService) {
        this.apiService = apiService;
    }

    @GetMapping(
        value = "/dogs/{dogId}/photos",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Finds photo information for dog",
        notes = "The returned URLs are relative - so the server domain has to be added as prefix. \n" +
            "Returns an empty list, if dog does not exist.\n\n" +
            "The images behind the URL may not exist after a few weeks, if they've been deleted manually! ",
        response = List.class, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = Dog.class),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<Photo>> getPhotoDataByDog(@PathVariable("dogId") Long dogId) {

        List<Photo> photos = apiService.getPhotosByDog(dogId);

        // Generate image URL for each photo entity
        for (Photo photo : photos) {
            // explicitly not a full URL, the app has to add the base URL by itself
            generateUrl(photo);
        }

        return new ResponseEntity<>(photos, HttpStatus.OK);
    }

    private void generateUrl(Photo photo) {
        String urlPart = PHOTOS_IMAGE_URL + photo.getFileName();
        photo.setUrl(urlPart);
    }

    @GetMapping(
        value = "/photos/image/{fileName}",
        produces = MediaType.IMAGE_JPEG_VALUE)
    @ApiOperation(value = "Returns photo as stream",
        notes = "The URL can be used as if the photo would be on a CDN / server filesystem. \n\n" +
            "The images behind the URL may not exist after a few weeks, if they've been deleted manually!",
        response = List.class, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = byte[].class),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error"),
        @ApiResponse(code = 502, message = "Bad Gateway (example: error while accessing file system)")
    })
    public @ResponseBody
    byte[] getPhotoData(@PathVariable("fileName") String fileName)
        throws PhotoFileConversionException, NoSuchFileException {

        return apiService.getPhotoData(fileName);
    }

    @PostMapping(
        value = "/dogs/{dogId}/photos",
        produces = MediaType.APPLICATION_JSON_VALUE
    )
    @ApiOperation(value = "Creates new photo metadata and persists photo on filesystem",
        notes = "Returns part of image URL (without domain). e.g. 2021-01-12-11-52-53-419_1.jpg \n" +
            "The last number (..._x.jpg) is thedog ID the photo belongs to.",
        response = Photo.class, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = Photo.class),
        @ApiResponse(code = 404, message = "Not Found. Element does not exist."),
        @ApiResponse(code = 500, message = "Internal Server Error"),
        @ApiResponse(code = 502, message = "Bad Gateway (example: error while accessing file system)")
    })
    public Photo postImage(
        @PathVariable("dogId") Long dogId,
        @RequestParam("image") MultipartFile multipartFile) {

        Photo photo = apiService.createPhoto(dogId, multipartFile);
        generateUrl(photo);

        return photo;
    }

    @DeleteMapping(
        value = "/photos/{id}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Deletes photo file + metadata",
        notes = "Returns id of the deleted photo.",
        response = Long.class, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = Long.class),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Long> deletePhoto(@PathVariable("id") Long photoId) {
        return new ResponseEntity<>(apiService.deletePhoto(photoId), HttpStatus.OK);
    }
}
