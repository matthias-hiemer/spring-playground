package com.example.demo.api;

import com.example.demo.business.IDogService;
import com.example.demo.model.Dog;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController(value = "DogController")
public class DogController {

    private final IDogService dogService;

    public DogController(IDogService dogService) {
        this.dogService = dogService;
    }

    @GetMapping(
        value = "/dogs",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Finds all dogs",
        notes = "gets all the dogs, cute and less cute ones.",
        response = List.class, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = List.class),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Iterable<Dog>> getDogs() {
        return new ResponseEntity<>(dogService.getDogs(), HttpStatus.OK);
    }

    @GetMapping(
        value = "/dogs/{id}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Finds dog by ID",
        notes = "Returns error if id does not exist.",
        response = Dog.class, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", responseContainer = "List", response = Dog.class),
        @ApiResponse(code = 404, message = "Not Found"),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Dog> getById(@PathVariable("id") Long id) {
        Dog dog = dogService.getById(id)
            .orElseThrow(() -> new NoSuchElementException("Dog not found! id: " + id));

        return new ResponseEntity<>(dog, HttpStatus.OK);
    }

    @PostMapping(
        value = "/dogs",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Creates new dog.",
        notes = "Returns newly created dog. \n",
        response = Dog.class, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = Dog.class),
        @ApiResponse(code = 412, message = "Precondition Failed (example: Optimistic Lock Exception"),
        @ApiResponse(code = 422, message = "Unprocessable Entity (example: id in entity and id in URL don't match)"),
        @ApiResponse(code = 502, message = "Bad Gateway"),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Dog> create(@RequestBody Dog newDog) {
        return new ResponseEntity<>(dogService.createDog(newDog), HttpStatus.OK);
    }
    @PostMapping(
        value = "/dogs/generate",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Generates new random dog.",
        notes = "Returns newly generated dog. \n",
        response = Dog.class, tags = {})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Ok", response = Dog.class),
        @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<Dog> generateRandomDog() {
        return new ResponseEntity<>(dogService.generateRandomDog(), HttpStatus.OK);
    }



}
