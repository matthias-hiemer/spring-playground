package com.example.demo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@Slf4j
@ComponentScan
@SpringBootApplication
public class DemoApplication {

    public static void main(String[] args) {
        // Surround with try catch to log exceptions that would normally disappear magically.
        try {
            SpringApplication.run(DemoApplication.class, args);

        } catch (Throwable t) {
            log.error("An error was thrown by starting the Spring application!", t);
        }
    }

}
