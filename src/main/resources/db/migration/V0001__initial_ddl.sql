CREATE TABLE dog (
    id BIGINT NOT NULL PRIMARY KEY auto_increment,
    version BIGINT NOT NULL DEFAULT 0,
    name VARCHAR NOT NULL,
    birthday DATE NULL,

    created_date timestamp NOT NULL,
    updated_date timestamp NOT NULL
);

CREATE TABLE photo (
    id BIGINT NOT NULL PRIMARY KEY auto_increment,
    created_date timestamp NOT NULL,
    file_name VARCHAR(256) NOT NULL UNIQUE,

    dog_id BIGINT NOT NULL,
    foreign key (dog_id) references dog(id)
);
