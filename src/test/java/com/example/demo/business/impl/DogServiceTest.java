package com.example.demo.business.impl;

import static org.mockito.Mockito.mock;

import com.example.demo.business.IDogDataGenerationService;
import com.example.demo.business.IDogService;
import com.example.demo.model.Dog;
import com.example.demo.repo.DogRepository;
import java.time.LocalDate;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class DogServiceTest {

    private final DogRepository dogRepository = mock(DogRepository.class);
    private final IDogDataGenerationService dogDataGenService = mock(IDogDataGenerationService.class);

    private final IDogService dogService = new DogService(dogRepository, dogDataGenService);

    @Test
    public void generateDog_whenInvalidData_validationFails() {
        Dog futureDog = new Dog()
            .withName("Major Methusalem")
            .withBirthday(LocalDate.now().plusWeeks(1));

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            dogService.createDog(futureDog);
        });
    }

}
