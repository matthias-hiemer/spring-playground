package com.example.demo.business.impl;

import static org.assertj.core.api.Assertions.assertThat;

import com.example.demo.business.IDogDataGenerationService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class DogDataGenerationServiceTest {

    private final IDogDataGenerationService dogDataGenService = new DogDataGenerationService();

    @Test
    public void generateName_success() {
        String name = dogDataGenService.generateName();

        assertThat(name).isNotEmpty();
        assertThat(name).matches("^(\\w* )*\\w*$");
    }
}
