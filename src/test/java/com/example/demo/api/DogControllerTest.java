package com.example.demo.api;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.example.demo.model.Dog;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.time.LocalDate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.util.Base64Utils;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@WithMockUser
class DogControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void callWithWrongAuth_failsWith401() throws Exception {

        // Wrong auth = 401
        MockHttpServletRequestBuilder builder =
            MockMvcRequestBuilders.get("/dogs")
                .header(HttpHeaders.AUTHORIZATION,
                    "Basic " + Base64Utils.encodeToString("wronguser:wrongsecret".getBytes()));

        this.mockMvc.perform(builder)
            .andDo(print())
            .andExpect(status().isUnauthorized());
    }

    @Test
    void postDog_whenValidData_returnsValidDog() throws Exception {
        LocalDate birthday = LocalDate.now().minusMonths(6);
        Dog dog = new Dog()
            .withName("Emporer Fluffer")
            .withBirthday(birthday);

        MockHttpServletRequestBuilder builder =
            MockMvcRequestBuilders.post("/dogs")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8")
                .content(objectMapper.writeValueAsString(dog));

        this.mockMvc.perform(builder)
            .andDo(print())
            .andExpect(status().isOk())
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Emporer Fluffer"))
            .andExpect(MockMvcResultMatchers.jsonPath("$.birthday").value(birthday.toString()));
    }
}
